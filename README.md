# Middleman::Firebase

This gem contains some basic Rake tasks to automate deployment of [Middleman](http://www.middlemanapp.com) sites to [Firebase Hosting](https://firebase.google.com/docs/hosting/).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'middleman-firebase'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install middleman-firebase

## Usage

Add the following line to your Rakefile:

```ruby
require 'middleman/firebase/tasks'
```

If you now list your Rake tasks with `rake -T`, you'll see several tasks in the `firebase` namespace, as well as `middleman:build` (which just builds the site into the `build` directory). The most useful of these tasks is probably `firebase:deploy`, which (if you have your Firebase CLI client configured) will build the Middleman site, push it to Firebase, and make a Git tag for the deployed version.

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/marnen/middleman-firebase.  (I might move to GitHub at some point, since I like it better; the Microsoft acquisition is causing some uncertainty for me.)


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
